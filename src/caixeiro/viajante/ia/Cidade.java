package caixeiro.viajante.ia;

public class Cidade {

    public int cidade, x, y;

    public Cidade(int cidade, int x, int y) {
        this.cidade = cidade;
        this.x = x;
        this.y = y;
    }

    public int getcidade() {
        return cidade;
    }

    public void setcidade(int cidade) {
        this.cidade = cidade;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

}
