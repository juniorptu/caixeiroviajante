package caixeiro.viajante.ia;

import java.util.ArrayList;
import java.util.List;
import static caixeiro.viajante.ia.CaixeiroViajante.gerarCustosPorCidade;

public class Rota implements Comparable<Rota> {

    public List<Cidade> cidades = new ArrayList();
   

    public Rota(ArrayList<Cidade> cidades) {
        this.cidades = cidades;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }

    public double tamRota() {
        double aptidao = 0;
        Cidade ultima = cidades.get(0);
        for (Cidade c : cidades) {
            if (ultima != c) {
                aptidao += gerarCustosPorCidade(ultima, c);
            }
            ultima = c;
        }
        return aptidao;
    }

     @Override
     public String toString(){
        String ret = "";
        for(Cidade cidade : cidades){
           ret += cidade.cidade + " -> ";   
        }
        ret += " DISTANCIA TOTAL = " + this.tamRota();
        return ret;
    }

    @Override
    public int compareTo(Rota ind) {
        if (this.tamRota() > ind.tamRota()) {
            return 1;
        } else if (this.tamRota() < ind.tamRota()) {
            return -1;
        } else {
            return 0;
        }
    }
}
