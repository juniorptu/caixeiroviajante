package caixeiro.viajante.ia;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class CaixeiroViajante {

    static final int NUMERO_CIDADES = 15;
    static final int NUMERO_POPULACAO = 50;
    static final int GERACOES = 500;
    static int cont = 1;

    public static void main(String[] args) {

        ArrayList<Rota> populacao = gerarPopulacao(NUMERO_CIDADES, NUMERO_POPULACAO, 30, 30);
        for (int i = 0; i < GERACOES; i++) {
            populacao = reproducao(populacao);
            for (Rota rota : populacao) {

                System.err.println(rota);
            }
            System.err.println("");
            System.err.println("");
        }

        Collections.sort(populacao);
        System.err.println("MELHOR ROTA APÓS " + "GERAÇÕES " + GERACOES);
        System.err.println(populacao.get(0));

    }

    public static double gerarCustosPorCidade(Cidade cidade1, Cidade cidade2) {
        double y, x, total;
        x = Math.abs(cidade1.x - cidade2.x);
        y = Math.abs(cidade1.y - cidade2.y);
        total = Math.sqrt((Math.pow((double) y, 2) + Math.pow((double) x, 2)));
        return total;
    }

    public static Cidade gerarCidade(int cidade, int distanciaX, int distanciaY) {
        int x = (int) (Math.random() * (distanciaX));
        System.err.print("CIDADE " + cont + " X = " + x);
        int y = (int) (Math.random() * (distanciaY));
        System.err.println(" Y = " + y);
        System.err.println();
        cont++;
        return new Cidade(cidade, x, y);

    }

    public static ArrayList<Rota> gerarPopulacao(int cidadeCidades, int cidadePopulacao, int distanciaX, int distanciaY) {
        ArrayList<Rota> populacaoLocal = new ArrayList<>();
        ArrayList<Cidade> cidades = new ArrayList();
        for (int i = 1; i <= cidadeCidades; i++) {
            cidades.add(gerarCidade(i, distanciaX, distanciaY));
        }
        for (int i = 0; i < cidadePopulacao; i++) {
            ArrayList<Cidade> novasCidades = new ArrayList<>();
            for (Cidade c : cidades) {
                novasCidades.add(new Cidade(c.getcidade(), c.getX(), c.getY()));
            }
            Collections.shuffle(novasCidades);
            populacaoLocal.add(new Rota(novasCidades));
        }
        return populacaoLocal;
    }

    public static Rota torneio(Rota ind1, Rota ind2) {
        if (ind1.tamRota() < ind2.tamRota()) {
            return ind1;
        } else {
            return ind2;
        }
    }
    

    public static Rota[] crossOver(Rota ind1, Rota ind2) {
        ArrayList<Cidade> cidades1 = new ArrayList();
        ArrayList<Cidade> cidades2 = new ArrayList();
        int pontoCorte = (int) (Math.random() * (ind1.cidades.size() - 1));
        if (pontoCorte == 0) {
            pontoCorte = (int) ind1.cidades.size() / 2;
        }
        for (Cidade c : ind1.cidades.subList(0, pontoCorte)) {
            cidades1.add(new Cidade(c.getcidade(), c.getX(), c.getY()));
        }
        for (Cidade c : ind2.cidades.subList(pontoCorte, ind2.cidades.size())) {
            cidades1.add(new Cidade(c.getcidade(), c.getX(), c.getY()));
        }
        for (Cidade c : ind1.cidades.subList(pontoCorte, ind1.cidades.size())) {
            cidades2.add(new Cidade(c.getcidade(), c.getX(), c.getY()));
        }
        for (Cidade c : ind2.cidades.subList(0, pontoCorte)) {
            cidades2.add(new Cidade(c.getcidade(), c.getX(), c.getY()));
        }

        Rota[] Rotas = new Rota[2];
        Rotas[0] = new Rota(cidades1);
        Rotas[1] = new Rota(cidades2);
        return Rotas;
    }
    
      public static ArrayList<Rota> reproducao(ArrayList<Rota> populacao) {
        Random numAleatorio = new Random();
        ArrayList<Rota> populacaoLocal = new ArrayList();
        for (int i = 0; i < ((int) populacao.size() / 2); i++) {
           
            Rota ind1 = torneio(populacao.get(numAleatorio.nextInt(populacao.size())), populacao.get(numAleatorio.nextInt(populacao.size())));
            Rota ind2 = torneio(populacao.get(numAleatorio.nextInt(populacao.size())), populacao.get(numAleatorio.nextInt(populacao.size())));
            Rota[] filhos = crossOver(ind1, ind2);
            filhos[0].cidades = alterarRepetidos((ArrayList<Cidade>) filhos[0].cidades,(ArrayList<Cidade>) populacao.get(0).cidades);
            filhos[1].cidades = alterarRepetidos((ArrayList<Cidade>) filhos[1].cidades,(ArrayList<Cidade>) populacao.get(0).cidades);
            populacaoLocal.add(mutacao(filhos[0], 0.1));
            populacaoLocal.add(mutacao(filhos[1], 0.1));
        }
        return elitismo(populacao, populacaoLocal);
    }

    public static Rota mutacao(Rota Rota, double probabilidade) {
        double x = Math.random() * (100);
        if (x <= probabilidade) {
            Collections.shuffle(Rota.cidades);
        }
        return Rota;
    }


    public static ArrayList<Rota> elitismo(ArrayList<Rota> populacao, ArrayList<Rota> filhos) {
    
        Collections.sort(filhos);
        Collections.sort(populacao);
        filhos.get(filhos.size() - 1).cidades = populacao.get(0).cidades;
        filhos.get(filhos.size() - 2).cidades = populacao.get(1).cidades;
        //System.err.println("POPULACAO " + populacao.get(0));
        //System.err.println("FILHOS " +  filhos.get(filhos.size() - 1));
        return filhos;
    }

  
    
      public static ArrayList<Cidade> alterarRepetidos(ArrayList<Cidade> cidades, ArrayList<Cidade> todasCidades) {
        ArrayList<Integer> cidadesFaltando = new ArrayList();
        ArrayList<Integer> cidadesRepetidos = new ArrayList();
        for (int i = 1; i <= cidades.size(); i++) {
            int cont2 = 0;
            for (Cidade cid : cidades) {
                if (i == cid.getcidade()) {
                    cont2++;
                }
            }
            if (cont2 == 0) {
                cidadesFaltando.add(i);
            } else if (cont2 == 2) {
                cidadesRepetidos.add(i);
            }
        }
        if (cidadesFaltando.size() > 0) {
            for (Cidade cid : cidades) {
                if (cidadesRepetidos.contains(cid.getcidade())) {
                    cidadesRepetidos.remove(cidadesRepetidos.indexOf(cid.getcidade()));
                    for (Cidade c : todasCidades) {
                        if (c.getcidade() == cidadesFaltando.get(0)) {
                            cid.cidade = cidadesFaltando.remove(0);
                            cid.x = c.getX();
                            cid.y = c.getY();
                            break;
                        }
                    }
                }
            }
        }
            return cidades;
    }

}
